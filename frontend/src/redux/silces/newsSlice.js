import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  title: null,
  text: null,
  genre: 'Politic',
  isPrivate: false,
  createDate: null,
  loading: false,
  author: null,
}

const newSlice = createSlice({
  name: "news",
  initialState,

  reducers: {
    setNews: (state, action) => {
      state.title = action.payload.title;
      state.text = action.payload.text;
      state.genre = action.payload.genre;
      state.isPrivate = action.payload.isPrivate === 1 ? true : false;
      state.createDate = action.payload.createDate;
      state.author = action.payload.author;
    },
    setLoading(state, action) {
      state.loading = action.payload;
    },
  }
});

export const { setNews, setLoading } = newSlice.actions;


export const fetchOneItems = (id) => async (dispatch) => {
  dispatch(setLoading(true));
  try {

    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`http://localhost:8000/api/newsposts/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    console.log(res)
    if (!res.ok) {
      throw new Error('Failed to fetch items');
    }
    const data = await res.json();
    console.log(data.data)
    dispatch(setNews(data.data));
  } catch (error) {
    console.error('Error fetching items:', error);
  } finally {
    dispatch(setLoading(false));
  }
}
export default newSlice.reducer;

