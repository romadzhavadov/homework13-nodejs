import { configureStore } from "@reduxjs/toolkit";
import itemsReducer from "./silces/itemsSlice";
import newsReducer from "./silces/newsSlice";
import tokenReducer from "./silces/tokenSlice"

const store = configureStore({
  reducer: {
    items: itemsReducer,
    news: newsReducer,
    token: tokenReducer
  },
})

export default store;