import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import styles from './NewsDetailsPage.module.scss';
import { useDispatch } from "react-redux";
import { fetchOneItems } from '../../redux/silces/newsSlice';
import {shallowEqual, useSelector } from "react-redux";

const NewsDetailsPage = () => {
  const { id } = useParams();

  const { title, text, genre, author} = useSelector(state => state.news, shallowEqual)
  
  console.log(author)
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchOneItems(id))
  }, [id]);


  return (
    <div className={styles.container}>
      {id ? (
        <div className={styles.newsDetails}>
          <h2 className={styles.newsTitle}>{title}</h2>
          <p className={styles.newsText}>{text}</p>
          {author && <div className={styles.newsText}>Author: {author.email}</div>}
          <div className={styles.genre}>Topic: {genre}</div>
          <div className={styles.btnGroup}>
            <Link to={`/edit/${id}`}  >
              <button>Редагувати новину</button>
            </Link>
            <Link to="/home" >
              <button>Повернутися до списку новин</button>  
            </Link>
          </div>
        </div>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

export default NewsDetailsPage;
