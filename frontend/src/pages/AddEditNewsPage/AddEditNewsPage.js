import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import styles from './AddEditNewsPage.module.scss';
import axios from 'axios';
import { shallowEqual, useSelector, useDispatch} from "react-redux";
import { fetchItems } from '../../redux/silces/itemsSlice';
import { fetchOneItems } from '../../redux/silces/newsSlice';

const AddEditNewsPage = () => {

  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const data = useSelector(state => state.news, shallowEqual);
  console.log(data)

  const [title, setTitle] = useState('');
  const [text, setText] = useState('');
  const [genre, setGenre] = useState('Politic'); 
  const [isPrivate, setIsPrivate] = useState(false); 
  const [err, setErr] = useState(null);

useEffect(() => {
  if (id) {
    dispatch(fetchOneItems(id))

    if (data) {
      setTitle(data.title);
      setText(data.text);
      setGenre(data.genre); 
      setIsPrivate(data.isPrivate); 
    }
  }
}, [id]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const newsPost = {
      title: title,
      text: text,
      genre: genre,
      isPrivate: isPrivate,
    };

    console.log(newsPost)

    try {
      const token = localStorage.getItem('TOKEN');
      if (id) {
        const newsData = await axios.put(`http://localhost:8000/api/newsposts/${id}`, newsPost, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        console.log(newsData)
      } else {
        await axios.post('http://localhost:8000/api/newsposts', newsPost, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        });
        console.log(newsPost)
      }
      await dispatch(fetchItems())
      navigate('/home');
    } catch (error) {
      console.error('Error saving news post:', error);
      console.log(error.response.data)
      setErr(error.response ? error.response.data : "Unknown error")
    } 
  };

  return (
    <div className={styles.container}>
      <h2>{id ? 'Редагування' : 'Створення'} новини</h2>
      <form onSubmit={handleSubmit}>
        <div className={styles.wrap}>
          <label className={styles.item}>Заголовок:</label>
          <input className={styles.item} type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
        </div>
        <div className={styles.wrap}>
          <label className={styles.item}>Текст:</label>
          <textarea className={styles.item} value={text} onChange={(e) => setText(e.target.value)}></textarea>
        </div>
        <div className={styles.wrap}>
          <label className={styles.item}>Жанр:</label>
          <select className={styles.item} value={genre} onChange={(e) => setGenre(e.target.value)}>
            <option value="Politic">Politic</option>
            <option value="Business">Business</option>
            <option value="Sport">Sport</option>
            <option value="Other">Other</option>
          </select>
        </div>
        <div className={styles.wrap}>
          <label className={styles.item}>
            Приватна:
            <input type="checkbox" checked={isPrivate} onChange={(e) => setIsPrivate(e.target.checked)} />
          </label>
        </div>
        <div className={styles.btnGroup}>
          <button type="submit">Зберегти</button>
          <button onClick={() => navigate('/')}>Повернутися до списку новин</button>
        </div>
      </form>
      {err ? alert(`${err.error} Try again`) : null}
    </div>
  );
};

export default AddEditNewsPage;