import './App.scss';
import Header from "./comonents/Header";
import AppRoutes from "./AppRoutes";


const App = () => {

  return (
    <div className="App">
        <Header />
        <AppRoutes />
    </div>
  );
}

export default App;
