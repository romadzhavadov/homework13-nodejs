import UserRepositoryOrm from "./userRepositoryOrm"
import NewspostRepositoryOrm from "./newspostRepositoryOrm"

export {
    UserRepositoryOrm,
    NewspostRepositoryOrm
}