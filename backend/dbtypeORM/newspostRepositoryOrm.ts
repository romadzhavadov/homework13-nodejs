import { AppDataSource } from "./dataSource";
import { User } from "./entity/User";
import { Newspost } from "./entity/Newpost";

// const userRepositiry = AppDataSource.getRepository(User)

// клас DatabaseService це DAL (Data Access Layer) для роботи з базою даних
class NewspostRepositoryOrm {
  private static instance: NewspostRepositoryOrm;
  private db: any; 

  constructor() {
    this.db = AppDataSource.getRepository(Newspost);
  }


  // ініціалізація екземпляра класу(патерн Singleton)
  public static getInstance(): NewspostRepositoryOrm {
    if (!NewspostRepositoryOrm.instance) {
      NewspostRepositoryOrm.instance = new NewspostRepositoryOrm();
    }
    return NewspostRepositoryOrm.instance;
  }


// // метод для створення нового запису у базі даних для вказаної таблиці
async create(data: any) {
  try {
    const newspost = new Newspost();
    newspost.title = data.body.title;
    newspost.text = data.body.text;
    newspost.author = data.user.id
    
    const newPostSaved = await this.db.manager.save(newspost);
    console.log('New newspost saved:', newPostSaved);

    return newPostSaved;
  } catch (error) {
    console.error('Error creating new newspost:', error);
    throw error;
  }
}

async readAll(params: { page: number, size: number }) {
  try {
    const offset = (params.page - 1) * params.size;

    // Зчитування елементів з бази даних
    const items = await this.db.find({
      relations: ["author"],
      take: params.size,
      skip: offset,
    });

    // Зчитування загальної кількості елементів
    const count = await this.db.manager.count(Newspost);
    console.log("Всі НОВИНИ", items)
    return {
      items,
      count,
    };
  } catch (error) {
    console.error("Error reading data:", error);
    throw error;
  }
}

  // метод для отримання одного запису з бази даних
  async read(id: number) {
    try {
      const getOneItem = await this.db.findOne({ relations: ["author"], where: { id } });
      return getOneItem;
    } catch (error) {
      console.error("Error reading item:", error);
      throw error;
    }
  }

  // метод для оновлення одного запису
  async update(id: number, newData: any) {
    try {
      const itemToUpdate = await this.db.findOne({ where: { id } });
      const updatedItem = await this.db.save({ ...itemToUpdate, ...newData });
      
      return updatedItem;
    } catch (error) {
      console.error("Error updating item:", error);
      throw error;
    }
  }

  // // метод для видалення одного запису
  async delete(id: number) {
    try {
      const itemToDelete = await this.db.findOne({ where: { id } });
  
      await this.db.delete(itemToDelete);
  
      return { message: `Item with id ${id} deleted successfully` };
    } catch (error) {
      console.error("Error updating item:", error);
      throw error;
    }
  }
}

// експортування екземпляра класу
export default NewspostRepositoryOrm.getInstance();




