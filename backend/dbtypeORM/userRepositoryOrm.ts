import { User } from "./entity/User";
import { AppDataSource } from "./dataSource";

// const userRepositiry = AppDataSource.getRepository(User)

// клас DatabaseService це DAL (Data Access Layer) для роботи з базою даних
class UserRepositoryOrm {
  private static instance: UserRepositoryOrm;
  private db: any; 

  constructor() {
    this.db = AppDataSource.getRepository(User);
  }

  // ініціалізація екземпляра класу(патерн Singleton)
  public static getInstance(): UserRepositoryOrm {
    if (!UserRepositoryOrm.instance) {
      UserRepositoryOrm.instance = new UserRepositoryOrm();
    }
    return UserRepositoryOrm.instance;
  }

  // метод запису нового  юзера
  async registerUser(data: any) {
    try {
      const user = new User();
      user.email = data.email;
      user.password = data.password;
      const newUserSaved = await this.db.manager.save(user);
      return newUserSaved;
    } catch (error) {
      console.error('Error creating new user:', error);
      throw error;
    }
  }

  async getUser(email: any) {
    try {
      const getOneUser = await this.db.findOne({ where: { email } });
      return getOneUser;
    } catch (error) {
      console.error("Error reading item:", error);
      throw error;
    }
  }


}

// експортування екземпляра класу
export default UserRepositoryOrm.getInstance();




