import { Request, Response } from 'express';
import UserRepositoryOrm from '../dbtypeORM/userRepositoryOrm';
import NewspostRepositoryOrm from '../dbtypeORM/newspostRepositoryOrm';
import hashPassword from '../auth/hashPassword';
import createToken from '../auth/createToken';


export class BaseService {
 
    constructor( ) {
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.loginUser = this.loginUser.bind(this);
    }

    async getItems (req: Request, res: Response) {

        const page = Number(req.query.page) || 1
        const size = Number(req.query.size) || 4
 
        const data = await NewspostRepositoryOrm.readAll( 
            {
                page,
                size,
            }
        );

        return data
    }
    
    async getOneItems (req: Request, res: Response) {
        const data = await NewspostRepositoryOrm.read(Number(req.params.id));
        return data
    }
    
    async addNews (req: Request, res: Response) {
        const data = await NewspostRepositoryOrm.create(req);
        return data
    }
    
    async updateNews (req: Request, res: Response) {
        const data = await NewspostRepositoryOrm.read(Number(req.params.id));
        if (!data) {
            return { errorMessage: "News not found" };
        }
        const updated = await NewspostRepositoryOrm.update(Number(req.params.id), req.body);
        return updated
    }
    
    async deleteNews (req: Request, res: Response) {
        const data = await NewspostRepositoryOrm.read(Number(req.params.id));
        if (!data) {
            return { errorMessage: "News not found" };
        }

        await NewspostRepositoryOrm.delete(Number(req.params.id));
        return { message: `News was  removed` }
    }

    async registerUser(req: Request, res: Response) {
        // Валідація даних користувача
        const { email, password, confirmPassword } = req.body;

        // Перевірка, чи співпадають паролі
        if (password !== confirmPassword) {
            return { errorMessage: "Passwords do not match" };
        }

        if (email) {
            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!emailRegex.test(email)) {
                return { errorMessage: "Invalid email format" };
            }
        }

        const hashedPassword = hashPassword(password)
        const data = {
            email: email,
            password: hashedPassword
        }

        const user = await UserRepositoryOrm.registerUser(data);
        if (user) {
            return createToken(email, password)
        }
    }

    async loginUser(table: string, req: Request, res: Response) {
        const { email, password } = req.body;

        const user = await UserRepositoryOrm.getUser(email);

        if (!user) {
            throw new Error(`Item with email ${email} not found.`);
        }

        if (user.password === hashPassword(password)) {
            return createToken(email, password)
        }
    }

    // static async getUserByEmail(email: string) {
    //     const user = await UserRepositoryOrm.getUser( email);
    //     return user
    // }

    async getAuthUser(table: string, req: Request, res: Response) {
        const { email, password } = req.body;

        const user = await UserRepositoryOrm.getUser(email);
        return user
    }
}
