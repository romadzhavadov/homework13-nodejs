import express, { Application } from 'express';
import cors from 'cors';
import { AuthRoutes, ApiRoutes } from './routes';
import dotenv from 'dotenv';
import { logRequests, errorLogger, logger } from './logers/logger';
import "reflect-metadata";
import { AppDataSource } from './dbtypeORM/dataSource';

const PORT = process.env.PORT || 8000;

class App {
  private app: Application;

  constructor() {
    this.app = express();
    // this.configurrDbConnection()
  }

  configureDbConnection() {
    if (!AppDataSource.isInitialized) {
      AppDataSource.initialize()
        .then(() => {
          console.log("Data Source has been initialized!")
        })
        .catch((err) => {
          console.error("Error during Data Source initialization", err)
        })
    }
  }

  routing() {
    this.app.get('/', (req, res) => {
      res.sendFile('index.html', { root: './static' });
    });

    const initRoute = (route: string, Routes: any) => {
      Object.keys(Routes).forEach((key) => {
        if (route === "user") {
          this.app.use(`/${route}`, Routes[key]);
        }
        this.app.use(`/${route}/${key}`, Routes[key]);
        //   this.app.use(`/api/${key}`, Routes[key]);
      });
    }
    
    initRoute("auth", AuthRoutes);
    initRoute("api", ApiRoutes);
    initRoute("user", AuthRoutes);
  }

  initPlugins() {
    this.app.use(express.json());
    this.app.use(express.static('static'));
    this.app.use(cors());
    this.app.use(logRequests);
    this.app.use(errorLogger);
  }


  async start() {
    // if (process.env.NODE_ENV !== 'production') {
    //   await DatabaseService.createTables();
    // }
    this.configureDbConnection()
    this.initPlugins();
    this.routing();


    this.app.listen(PORT, () => {
      logger.info(`Server is running on port ${PORT}`);
    });
  }
}

dotenv.config(); 
const app = new App();
app.start(); 
