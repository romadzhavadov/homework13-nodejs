import Validator from 'fastest-validator';

export const validatorService = new Validator();

export const newspostSchema = {
  title: { type: "string", max: 50 },
  text: { type: "string", max: 256 },
  genre: { type: "string", enum: ['Politic', 'Business', 'Sport', 'Other'] },
  isPrivate: { type: "boolean" },
};

export const validateNewspost = validatorService.compile(newspostSchema);

