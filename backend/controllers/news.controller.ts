import { BaseController } from './base.controller';
import { NewsService } from '../service';

class NewsController extends BaseController {
  constructor() {
    const newsRepository = new NewsService()
    super('newsposts', newsRepository);
  }
  
}

export default NewsController;

